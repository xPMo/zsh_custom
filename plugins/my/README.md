As a workaround, I created `__tmux-sessions` and copied in the function from the `_tmux` completion (since `__tmux-sessions` isn't loaded until `_tmux` is callled initially).  `__tmux-sessions` is still autoloaded, so there's not a big problem.


* `tmax`: A wrapper for tmux which creates a session in the same session group provided (if no session provided, will pick one if one exists).  Upon detaching, kills the session.

* grc completion function (`\_grc`).

* `fzf-launch-widget` will let you search for valid commands.

* `editwhich` will try to find the source file of a given script or function. If `$EDITOR` is vi-like, will search for the function name if it is a function.

* `highlightwhich` uses `highlight` to output the given function or file.  It will try to highlight binary executables, so be careful.

my.plugin.zsh
my.plugin.zsh.zwc
README.md
