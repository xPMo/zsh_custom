if [[ $- == *i* ]]; then
	autoload -U __flaunch fzf-launch-widget _grc tmax __tmux-sessions editwhich highlightwhich lines
	if (( $+commands[pacman] )); then
		autoload -U pacman-autoremove
		compdef _pacman pacman-autoremove='pacman -R'
	fi
	zle -N fzf-launch-widget
	bindkey '^X' fzf-launch-widget
	(( ${plugins[(I)zsh-autosuggestions]} )) && bindkey '^ ' autosuggest-accept
	compdef __tmux-sessions tmax
	compdef _command_names highlightwhich
	compdef _command_names editwhich
fi
