DEFAULT_VI_MODE=viins

# Set cursor style
function __set_cursor_vi() {
	case $KEYMAP in
		# block
		vicmd) print -n "\e[2 q" ;;
		# bar
		*) print -n "\e[6 q" ;;
	esac
	zle reset-prompt
}

# Updates editor information when the keymap changes.
function zle-keymap-select() {
	__set_cursor_vi
	zle -R
}

# sets keymap on next commandline
zle-line-init() {
	zle -K $DEFAULT_VI_MODE
}
zle -N zle-line-init

# Ensure that the prompt is redrawn when the terminal size changes.
TRAPWINCH() {
	zle &&  zle -R
}

zle -N zle-keymap-select
zle -N edit-command-line


bindkey -v

# allow v to edit the command line (standard behaviour)
autoload -Uz edit-command-line
bindkey -M vicmd 'v' edit-command-line

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey '^P' up-history
bindkey '^N' down-history

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word

# allow ctrl-r to perform backward search in history
bindkey '^r' history-incremental-search-backward

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line
