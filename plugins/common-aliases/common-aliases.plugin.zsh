# NOTE: I split this from OMZ's common-aliases plugin
# to change default behaviour only:
# > use rg over ag
# > use rm -I over rm -i
# > add -i (prompt before removal) for crontab
# ===================
# Advanced Aliases.
# Use with caution
# 
#

# ls, the common ones I use a lot shortened for rapid fire usage
alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
alias ll='ls -l'      #long list
alias ldot='ls -ld .*'
alias lS='ls -1FSsh'
alias lart='ls -1Fcart'
alias lrt='ls -1Fcrt'

# Quick access to the .zshrc file
(( ${+ZDOTDIR} )) && alias zshrc='$EDITOR $ZDOTDIR/.zshrc' || alias zshrc='$EDITOR ~/.zshrc'

if (( $+commands[rg] )); then
	alias grep='rg -N'
	alias sgrep='rg -n -C 5'
elif (( $+commands[ag] )); then
	alias grep='ag --nonumbers'
	alias sgrep='ag -C 5'
else
	alias grep='grep --color'
	# use grep as a cheap rg/ag replacemnet
	alias ag='grep -R -n -H --exclude-dir={.git,.svn,CVS} '
	alias rg='grep -R -n -H --exclude-dir={.git,.svn,CVS} '
	alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '
fi

# Command line head / tail shortcuts
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L="| less"
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"
# alias -g P="2>&1| pygmentize -l pytb"

alias dud='du -d 1 -h'
alias duf='du -sh *'
if (( $+commands[fd] )); then
	alias find=fd
	alias fd='noglob fd --type directory'
	alias ff='noglob fd --type file'
	alias fl='noglob fd --type symlink'
else
	alias fd='noglob find . -type d -name'
	alias ff='noglob find . -type f -name'
	alias fl='noglob find . -type l -name'
fi

alias h='history'
alias hgrep="fc -El 0 | grep"
# alias help='man'
alias p='ps -f'
alias sortnr='sort -n -r'
alias unexport='unset'

alias about='id; hostname --long; cat /etc/*-release'
alias whereami=display_info

alias rm='rm -I'
alias cp='cp -i --reflink=auto' # lightweight copy when possible
alias mv='mv -i'
alias crontab='crontab -i'

# zsh is able to auto-do some kungfoo
# depends on the SUFFIX :)
if is-at-least 4.2.0; then
  # open browser on urls
  if [[ -n "$BROWSER" ]]; then
    _browser_fts=(htm html de org net com at cx nl se dk)
    for ft in $_browser_fts; do alias -s $ft=$BROWSER; done
  fi

  _editor_fts=(cpp cxx cc c hh h inl asc txt TXT tex)
  for ft in $_editor_fts; do alias -s $ft=$EDITOR; done

  if [[ -n "$XIVIEWER" ]]; then
    _image_fts=(jpg jpeg png gif mng tiff tif xpm)
    for ft in $_image_fts; do alias -s $ft=$XIVIEWER; done
  fi

  _media_fts=(ape avi flv m4a mkv mov mp3 mpeg mpg ogg ogm rm wav webm)
  for ft in $_media_fts; do alias -s $ft=mpv; done

  #read documents
  alias -s pdf=zathura
  alias -s ps=gv
  alias -s dvi=xdvi
  alias -s chm=xchm
  alias -s djvu=djview

  #list whats inside packed file
  alias -s zip="unzip -l"
  alias -s rar="unrar l"
  alias -s tar="tar tf"
  alias -s tar.gz="echo "
  alias -s ace="unace l"
fi

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(mosh|ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'
