# This theme is a fork of dieter from oh-my-zsh, with similar goals:
# Differences:
# > pwd shows current dir and parent dir
# > Shell depth at front if this is a nested shell
# > no timestamp
# the idea of this theme is to contain a lot of info in a small string, by
# compressing some parts and colorcoding, which bring useful visual cues,
# while limiting the amount of colors and such to keep it easy on the eyes.
# When a command exited >0, the timestamp will be in red and the exit code
# will be on the right edge.
# The exit code visual cues will only display once.
# (i.e. they will be reset, even if you hit enter a few times on empty command prompts)

typeset -A host_repr

# translate hostnames into shortened, colorcoded strings
host_repr=('parched' "%{$fg[green]%}parched" 'pbunt' "%{$fg[red]%}pbunt" 'alarmpi' "%{$fg[blue]%}pi" 'localhost' "%{$fg[cyan]%}local")

# local time, color coded by last return code
#time_enabled="%(?.%{$fg[green]%}.%{$fg[red]%})%* %{$reset_color%}"
#time_disabled="%{$fg[green]%}%* %{$reset_color%}"
#time=

# user part, color coded by privileges
local user="%(!.%{$fg_bold[red]%}.%{$fg[blue]%})%n%{$reset_color%}"

# Hostname part.  compressed and colorcoded per host_repr array
# if not found, regular hostname in default color
local host="@${host_repr[$HOST]:-$HOST}%{$reset_color%}"

# Compacted $PWD
local pwd="%{$fg[blue]%}%2c%{$reset_color%}"

# Full $PWD
# local pwd="%{$fg[blue]%}%~%{$reset_color%}"

# Newline prompt:
# local newline="%(?.%{$fg[green]%}.%{$fg_bold[red]%})γ %{$reset_color%}"
# "%{$fg[white]%}%{$reset_color%}"

# shell depth
function shell_depth() {
	# HISTFILE not set: incognito mode
	[ -n "$HISTFILE" ] || print -n "%{$fg[grey]%}|"
	local l=
	# TMUX session offset
	(( ${+TMUX} )) && (( l = SHLVL - 1 )) || l=$SHLVL
	[ $l -gt 1 ] && print "%{$fg[red]%}↣$l "
}

PROMPT='$(shell_depth)${time}${user}${host}:${pwd} $(git_prompt_info)'

# i would prefer 1 icon that shows the "most drastic" deviation from HEAD,
# but lets see how this works out
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%} %{$fg[yellow]%}?%{$fg[green]%}%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}"

# elaborate exitcode on the right when >0
return_code_enabled="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
return_code_disabled=
return_code=$return_code_enabled

# vi status on the right
vi_status_enabled="%{$fg[yellow]%} [% CMD]% %{$reset_color%}"
vi_status_disabled=
vi_status=$vi_status_enabled

RPS1='${${KEYMAP/vicmd/$vi_status}/(main|viins)/} ${return_code}'
zle-keymap-select(){
	RPS1='${${KEYMAP/vicmd/$vi_status}/(main|viins)/} ${return_code}'
	zle reset-prompt
}
function accept-line-or-clear-warning () {
	if [[ -z $BUFFER ]]; then
		time=$time_disabled
		return_code=$return_code_disabled
	else
		time=$time_enabled
		return_code=$return_code_enabled
	fi
	zle accept-line
}
zle -N accept-line-or-clear-warning
zle -N zle-keymap-select
bindkey '^M' accept-line-or-clear-warning
