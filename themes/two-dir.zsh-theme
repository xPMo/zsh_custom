# Two directories, colored by permissions
function color_pwd() {
	if (( $(stat -c "%u" . ) == UID )); then
		# owner
		print "%{\033[38;5;4m%}%2c"
	elif [[ -w . ]]; then
		# not owner, but have write permissions
		print "%{\033[38;5;3m%}%2c"
	else
		# no write permissions
		print "%{\033[38;5;5m%}%2c"
	fi
}

# Newline prompt:
# local newline="%(?.%{$fg[green]%}.%{\033[38;5;1m\033[1m%})γ %{\033[0m%}"
# "%{$fg[white]%}%{\033[0m%}"

ZSH_THEME_GIT_PROMPT_PREFIX=" %{\033[38;5;3m%} "
ZSH_THEME_GIT_PROMPT_SUFFIX=""
# without git_prompt_status:
# ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%} %{\033[38;5;3m%}?%{$fg[green]%}%{\033[0m%}"
ZSH_THEME_GIT_PROMPT_DIRTY=""
# ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="?"
ZSH_THEME_GIT_PROMPT_ADDED="+"
ZSH_THEME_GIT_PROMPT_DELETED="-"
ZSH_THEME_GIT_PROMPT_MODIFIED="*"

function git_info() {
	local info="$(git_prompt_info)"
	(( ${+info} )) && print -n "$info$(git_prompt_status)%{\033[0m%}" \
		|| print -n "$info%{\033[0m%}"
}

PROMPT='$(color_pwd)$(git_info) '

# elaborate exitcode on the right when >0
# return_code_enabled="%(?..%{\033[38;5;1m%}%? ↵%{\033[0m%})"
return_code_enabled="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
return_code_disabled=
return_code=$return_code_enabled

RPS1="${return_code}"

function accept-line-or-clear-warning () {
	if [[ -z $BUFFER ]]; then
		time=$time_disabled
		return_code=$return_code_disabled
	else
		time=$time_enabled
		return_code=$return_code_enabled
	fi
	zle accept-line
}
zle -N accept-line-or-clear-warning
zle -N zle-keymap-select
bindkey '^M' accept-line-or-clear-warning
